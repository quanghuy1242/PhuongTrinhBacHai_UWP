﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Microsoft.Toolkit.Uwp;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace DemoControl
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void btnGiai_Click(object sender, RoutedEventArgs e)
        {
            double a, b, c, delta;

            try
            {
                a = Convert.ToDouble(txtSoA.Text);
                b = Convert.ToDouble(txtSoB.Text);
                c = Convert.ToDouble(txtSoC.Text);
            }
            catch
            {
                txtketQua.Text = "Vui lòng chỉ nhập số và không bỏ trống";
                return;
            }

            delta = b * b - 4 * a * c;
            if (a == 0)
            {
                if (b == 0)
                {
                    if (c == 0)
                    {
                        txtketQua.Text = "Phương trình có vô số nghiệm";
                        return;
                    }
                    else
                    {
                        txtketQua.Text = "Phương trình vô nghiệm";
                        return;
                    }
                }
            }
            else
            {
                if(delta < 0)
                {
                    txtketQua.Text = "Phương trình vô nghiệm";
                    return;
                }
                if (delta > 0)
                {
                    double x1, x2;
                    x1 = (-b + Math.Sqrt(delta)) / (2 * a);
                    x2 = (-b - Math.Sqrt(delta)) / (2 * a);
                    txtketQua.Text = x1.ToString() + ", " + x2.ToString();
                    return;
                }
                else
                {
                    txtketQua.Text = ((-b) / (2 * a)).ToString();
                    return;
                }
            }
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(1);
        }

        private void btnXoaRong_Click(object sender, RoutedEventArgs e)
        {
            txtketQua.Text = "";
            txtSoA.Text = "";
            txtSoB.Text = "";
            txtSoC.Text = "";
        }

        private async void MenuFlyoutItem_Click(object sender, RoutedEventArgs e)
        {
            ContentDialog dialog = new ContentDialog()
            {
                Title = "Thông tin",
                Content="Phần mềm giải phương trình bậc hai\nQuang Huy\n2018",
                CloseButtonText = "OK"
            };
            await dialog.ShowAsync();
        }
    }
}
